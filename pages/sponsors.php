<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();	 

$result = Db::ExecuteFirst("SELECT * FROM sponsors ORDER BY linktext", $conn);

$sponsors_query = Db::ExecuteQuery("SELECT * FROM sponsors ORDER BY linktext", $conn);

$pagecontent = "";
foreach ($sponsors_query as $value) 
{
	
	$linktext = $value["linktext"];
	$linkurl = $value["linkurl"];
	$imgurl = $value["imgurl"];
	$id = $value["ID"];	
	
		$pagecontent .= '<div class="col-xs-12 col-md-6">
		
		<img class="image-sponsors" src="'.$imgurl.'" alt="" /><br /> 
		
		<p class="text-center"><strong style="font-size: 22px;">
		
		<a style="color: white;" href="'.$linkurl.'" target="_BLANK">'.$linktext.'</a>
		</strong></p>
		</div>';

}






/*<div class="col-xs-12 col-md-6">
	
	<img class="image-sponsors" src="{{ s.imgurl }}" alt="" /><br /> 
	
	<p class="text-center"><strong style="font-size: 22px;">
	
	<a style="color: white;" href="{{ s.linkurl }}" target="_BLANK">

	</a>
	</strong></p>
</div> */


/*$context["sponsors"] = $result;

echo $twig->render('sponsors.html', $context);*/

$context["body"] = $pagecontent;

echo $twig->render('sponsors.html', $context);