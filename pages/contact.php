<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$name = post("name");
	$email = post("email");
	$message = post("message");
	$body = <<<EOD
Name: $name
Email: $email
Message:
$message
EOD;
	if(mail(ADMIN_EMAIL, "Contact Request", $body, "From: " . NO_REPLY_EMAIL ))
	{
		$context["xmessage"] = "Message Sent";
	}
	else
	{
		$context["xmessage"] = "Message Failed To Send";
	}
}

echo $twig->render('contact.html', $context);