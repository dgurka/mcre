<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();	 

//DISPLAY FEATURED VIDEOS
$videos = Db::ExecuteQuery("SELECT * FROM  `videos` ORDER BY ID DESC LIMIT 16", $conn);

$renderpage = "<div><h2>Featured Videos</h2></div>";

$renderpage .= "<div class=\"row\">";

	$counter = 1;
	foreach ($videos as $i => $vids) 
	{
	

		$renderpage .= "<div align=\"center\" class=\"col-xs-12 col-md-6\">";
		$renderpage .= "<h4>".$vids['title']."</h4>";
		
		$renderpage .= preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"//www.youtube.com/embed/$2\"  allowfullscreen=\"yes\" frameborder=\"0\" height=\"295\" width=\"400\"></iframe>",$vids['link']);
		
		$renderpage .="</div>";
				
		if($counter == 2){
			$renderpage .= "</div><div class=\"row\">";
			$counter = 1;	
		} else {
			$counter++;
		}
	}

$renderpage .= "</div>";
// END DISPLAY FEATURED VIDEOS
/*
$renderpage .= "<div><h2>Video Index</h2></div>";

$casestudies = Db::ExecuteQuery("SELECT * FROM  `videos` WHERE menukey = 0 ORDER BY  menuorder ASC", $conn);


	$renderpage .= "<br />"; // escape first line
	$renderpage .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" valign=\"top\">";

	foreach ($casestudies as $i => $cs) 
	{
		if($cs['ID'] == "16"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['ID'] == "32"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['ID'] == "48"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		// is the ad copy out of date?
		//if($cs['OBSOLETE'] == "yes"){ //yup. don't display a link to it.
		//	$renderpage .= $cs['caseno'] . " - ". $cs['title'] . "<br />";
		if($cs['menukey'] == "0"){ // nope, its still good. display a link.
			$renderpage .= "&#8226; &nbsp; <a href=\"../../video/".$cs['ID']."\">". $cs['title'] . "</a><br />";
		}
		
	}

	$renderpage .= "</td></tr></table>";

*/

$context["body"] = $renderpage;

echo $twig->render('newvideos.html', $context);