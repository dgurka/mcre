<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$message = "Please enter a search term, for example, &quot;bolt&quot;";

if($_SERVER["REQUEST_METHOD"] == "POST"){
	
	$message = "";
	
	// Get the search variable from URL
	
	$var = @$_POST['query'];
	$tvar = preg_replace("/[^a-zA-Z0-9\s]/", "", $var);
	$trimmed = trim($tvar); //trim whitespace from the stored variable
	
	if ($trimmed == "" OR !isset($var)){	// check for an empty string and display a message.
		$message = "You have not entered a search term. Displaying all listings.";
	} 
	
	$conn = Db::GetNewConnection();	 
	// Build SQL Query  
	//$query = "SELECT * FROM adcopies WHERE body LIKE '%".$trimmed."%'"; old query to search the body only 
	//$query = "SELECT * FROM adcopies WHERE body LIKE '%".$trimmed."%' OR part_name LIKE '%".$trimmed."%' OR feeder_style LIKE '%".$trimmed."%'"; 
	//$query = "SELECT * FROM videos WHERE title LIKE '%".$trimmed."%' OR keywords LIKE '%".$trimmed."%' OR part_name LIKE '%".$trimmed."%' OR feeder_style LIKE '%".$trimmed."%'"; 
	$query = "SELECT * FROM videos WHERE title LIKE '%".$trimmed."%' OR keywords LIKE '%".$trimmed."%'"; 
	
	$videos = Db::ExecuteQuery($query, $conn);
	
	$renderpage = "<br />"; // escape first line
	$renderpage .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" valign=\"top\">";
	
	foreach ($videos as $i => $cs){
		if($cs['ID'] == "66"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['ID'] == "131"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['ID'] == "196"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		// is the ad copy out of date?
		/*if($cs['OBSOLETE'] == "yes"){ //yup. don't display a link to it.
			$renderpage .= "<strong>". $cs['caseno'] . "</strong> - ". $cs['title'] . "<br /> (Part: ". $cs['part_name'] . ", Feeder Style: ". $cs['feeder_style'] . ")<br />";*/
		if($cs['menukey'] == "0"){ // nope, its still good. display a link.
			//$renderpage .= "&#8226; &nbsp; <a href=\"../../video/".$cs['ID']."\">". $cs['title'] . "</a><br /> (Part: ". $cs['part_name'] . ", Feeder Style: ". $cs['feeder_style'] . ")<br />";
			$renderpage .= "&#8226; &nbsp; <a href=\"../../video/".$cs['ID']."\">". $cs['title'] . "</a><br />";
		}
	
	}
	
		$renderpage .= "</td></tr></table>";
		$searchq = $var;
		$title = "Search Results for: ".$var;
	} else {
		$renderpage = "";
		$searchq = "";
		$title = "Video Search";
	}

$context["title"] = $title;
$context["message"] = $message;
$context["searchq"] = $searchq;
$context["body"] = $renderpage;

echo $twig->render('videosearch.html', $context);