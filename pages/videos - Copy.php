<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();	 

$videos = Db::ExecuteQuery("SELECT * FROM  `videos` WHERE menukey = 0 ORDER BY menuorder ASC", $conn);

$renderpage = "<div class=\"row\">";

	$counter = 1;
	foreach ($videos as $i => $vids) 
	{
	

		$renderpage .= "<div align=\"center\" class=\"col-xs-12 col-md-6\">";
		$renderpage .= "<h4>".$vids['title']."</h4>";
		
		$renderpage .= preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"//www.youtube.com/embed/$2\"  allowfullscreen=\"yes\" frameborder=\"0\" height=\"315\" width=\"420\"></iframe>",$vids['link']);
		
		$renderpage .="</div>";
				
		if($counter == 2){
			$renderpage .= "</div><div class=\"row\">";
			$counter = 1;	
		} else {
			$counter++;
		}
	}

$renderpage .= "</div>";

$renderpage .= "<div align=\"center\" class=\"row\" style=\"padding-top:15px; padding-bottom:5px;\"><a class=\"btn btn-primary\" href=\"https://www.youtube.com/user/SpectrumAutomationCo\" target=\"_blank\">View More on Youtube...</a></div>";

$context["body"] = $renderpage;

echo $twig->render('videos.html', $context);