<?php

date_default_timezone_set('America/Denver');

function getString($key)
{
	if(isset($_POST[$key]))
		return $_POST[$key];
	else
		return "";
}

$order_type_0 = " ";
$order_type_1 = " ";
$order_type_2 = " ";
$order_type_3 = " ";
$order_type_4 = " ";
$order_type_5 = " ";
$order_type_6 = " ";
$order_type_7 = " ";
$order_type_8 = " ";
$order_type_9 = " ";

$transaction_type_0 = " ";
$transaction_type_1 = " ";
$transaction_type_2 = " ";
$transaction_type_3 = " ";

$county_0 = " ";
$county_1 = " ";
$county_2 = " ";
$county_3 = " ";
$county_4 = " ";
$county_5 = " ";

$order_type = getString("order_type");
$order_type = "order_type_" . $order_type;
$$order_type = "X";

$transaction_type = getString("transaction_type");
$transaction_type = "transaction_type_" . $transaction_type;
$$transaction_type = "X";

$county = getString("county");
$county = "county_" . $county;
$$county = "X";

$need_by_date = getString("need_by_month") . "/" . getString("need_by_day") . "/" . getString("need_by_year");

$min = 4;

$seller_names = str_replace("\n", "<br/>", getString("seller_names"));
if($seller_names != "")
{
	$sc = count(split($seller_names, "<br/>"));
	for($i = 0; $i++; $i < $min-$sc)
		$seller_names .= "<br/>";
}

$seller_h_and_w = (getString('seller_h_and_w') === "yes") ? "X" : " ";

$buyer_names = str_replace("\n", "<br/>", getString("buyer_names"));
if($buyer_names != "")
{
	$sc = count(split($buyer_names, "<br/>"));
	for($i = 0; $i++; $i < $min-$sc)
		$buyer_names .= "<br/>";
}

$buyer_h_and_w = (getString('buyer_h_and_w') === "yes") ? "X" : " ";

$property_information_address = getString("property_information_address");
$property_information_city = getString("property_information_city");
$property_information_state = getString("property_information_state");
$property_information_zip = getString("property_information_zip");
$property_information_parcel_id = getString("property_information_parcel_id");
$property_information_long_legal = getString("property_information_long_legal");
$property_information_description = getString("property_information_description");

$lender_information_loan_amount = getString("lender_information_loan_amount");
$lender_information_exceptions = (getString('lender_information_exceptions') === "yes") ? "X" : " ";
$lender_information_name = getString("lender_information_name");
$lender_information_address = getString("lender_information_address");
$lender_information_city = getString("lender_information_city");
$lender_information_state = getString("lender_information_state");
$lender_information_zip = getString("lender_information_zip");
$lender_information_attn_to = getString("lender_information_attn_to");
$lender_information_phone = getString("lender_information_phone");
$lender_information_fax = getString("lender_information_fax");
$lender_information_loan_number = getString("lender_information_loan_number");

$realestate_information_sale_price = getString("realestate_information_sale_price");
$realestate_information_name = getString("realestate_information_name");
$realestate_information_address = getString("realestate_information_address");
$realestate_information_city = getString("realestate_information_city");
$realestate_information_state = getString("realestate_information_state");
$realestate_information_zip = getString("realestate_information_zip");
$realestate_information_attn_to = getString("realestate_information_attn_to");
$realestate_information_phone = getString("realestate_information_phone");
$realestate_information_fax = getString("realestate_information_fax");

$additional_comments = getString("additional_comments");
$additional_your_name = getString("additional_your_name");
$additional_your_phone = getString("additional_your_phone");

require("Mailer.php");

$mailer = Mailer::GetMailer();
//$mailer->AddTo("joseph@enablepoint.com");
$mailer->AddTo("loandocs@embassytitle.com");
$mailer->AddBcc("frank@enablepoint.com");
$mailer->AddBcc("joseph@enablepoint.com");
$mailer->From = "no-reply@embassytitle.com";
$mailer->LoadBodyFromFile("work_template.html");
$mailer->Subject = "Title Request Submision";

$HasUpload = false;

if(isset($_FILES["property_information_long_legal"]) && $_FILES["property_information_long_legal"]["error"] == 0)
	$HasUpload = true;

$mailer->ReplaceBodyKey($order_type_0, "ORDER_TYPE_0");
$mailer->ReplaceBodyKey($order_type_1, "ORDER_TYPE_1");
$mailer->ReplaceBodyKey($order_type_2, "ORDER_TYPE_2");
$mailer->ReplaceBodyKey($order_type_3, "ORDER_TYPE_3");
$mailer->ReplaceBodyKey($order_type_4, "ORDER_TYPE_4");
$mailer->ReplaceBodyKey($order_type_5, "ORDER_TYPE_5");
$mailer->ReplaceBodyKey($order_type_6, "ORDER_TYPE_6");
$mailer->ReplaceBodyKey($order_type_7, "ORDER_TYPE_7");
$mailer->ReplaceBodyKey($order_type_8, "ORDER_TYPE_8");
$mailer->ReplaceBodyKey($order_type_9, "ORDER_TYPE_9");
$mailer->ReplaceBodyKey($transaction_type_0, "TRANSACTION_TYPE_0");
$mailer->ReplaceBodyKey($transaction_type_1, "TRANSACTION_TYPE_1");
$mailer->ReplaceBodyKey($transaction_type_2, "TRANSACTION_TYPE_2");
$mailer->ReplaceBodyKey($transaction_type_3, "TRANSACTION_TYPE_3");
$mailer->ReplaceBodyKey($county_0, "COUNTY_0");
$mailer->ReplaceBodyKey($county_1, "COUNTY_1");
$mailer->ReplaceBodyKey($county_2, "COUNTY_2");
$mailer->ReplaceBodyKey($county_3, "COUNTY_3");
$mailer->ReplaceBodyKey($county_4, "COUNTY_4");
$mailer->ReplaceBodyKey($county_5, "COUNTY_5");
$mailer->ReplaceBodyKey(date('m/d/Y'), "TODAYS_DATE");
$mailer->ReplaceBodyKey($need_by_date, "NEEDBYE_DATE");
$mailer->ReplaceBodyKey($seller_names, "SELLER_NAMES");
$mailer->ReplaceBodyKey($seller_h_and_w, "SELLER_HUSBAND_YES");
$mailer->ReplaceBodyKey(($seller_h_and_w == "X")? " ": "X", "SELLER_HUSBAND_NO");
$mailer->ReplaceBodyKey($buyer_names, "BUYER_NAMES");
$mailer->ReplaceBodyKey($buyer_h_and_w, "BUYER_HUSBAND_YES");
$mailer->ReplaceBodyKey(($buyer_h_and_w == "X")? " ": "X", "BUYER_HUSBAND_NO");
$mailer->ReplaceBodyKey($property_information_address, "PROPERTY_ADDRESS");
$mailer->ReplaceBodyKey($property_information_city, "PROPERTY_CITY");
$mailer->ReplaceBodyKey($property_information_state, "PROPERTY_STATE");
$mailer->ReplaceBodyKey($property_information_zip, "PROPERTY_ZIP");
$mailer->ReplaceBodyKey($property_information_parcel_id, "PROPERTY_PARCEL_ID");
$mailer->ReplaceBodyKey(($HasUpload)? "X": " ", "HAS_LONG_ACREAGE");
$mailer->ReplaceBodyKey($property_information_description, "PROPERTY_DESCRIPTION");
$mailer->ReplaceBodyKey($lender_information_loan_amount, "LOAN_AMOUNT");
$mailer->ReplaceBodyKey($lender_information_exceptions, "WITH_EXEPTIONS_YES");
$mailer->ReplaceBodyKey(($lender_information_exceptions == "X")? " ": "X", "WITH_EXEPTIONS_NO");
$mailer->ReplaceBodyKey($lender_information_name, "LENDER_NAME");
$mailer->ReplaceBodyKey($lender_information_address, "LENDER_ADDRESS");
$mailer->ReplaceBodyKey($lender_information_city, "LENDER_CITY");
$mailer->ReplaceBodyKey($lender_information_state, "LENDER_STATE");
$mailer->ReplaceBodyKey($lender_information_zip, "LENDER_ZIP");
$mailer->ReplaceBodyKey($lender_information_attn_to, "LENDER_ATTN_TO");
$mailer->ReplaceBodyKey($lender_information_phone, "LENDER_PHONE");
$mailer->ReplaceBodyKey($lender_information_fax, "LENDER_FAX");
$mailer->ReplaceBodyKey($lender_information_loan_number, "LOAN_NUMBER");
$mailer->ReplaceBodyKey($realestate_information_sale_price, "SALE_PRICE");
$mailer->ReplaceBodyKey($realestate_information_name, "REAL_STATE_NAME");
$mailer->ReplaceBodyKey($realestate_information_address, "REAL_STATE_ADDRESS");
$mailer->ReplaceBodyKey($realestate_information_city, "REAL_STATE_CITY");
$mailer->ReplaceBodyKey($realestate_information_state, "REAL_STATE_STATE");
$mailer->ReplaceBodyKey($realestate_information_zip, "REAL_STATE_ZIP");
$mailer->ReplaceBodyKey($realestate_information_phone, "REAL_STATE_PHONE");
$mailer->ReplaceBodyKey($realestate_information_fax, "REAL_STATE_FAX");
$mailer->ReplaceBodyKey($realestate_information_attn_to, "REAL_STATE_ATTN_TO");
$mailer->ReplaceBodyKey($additional_comments, "ADDITIONAL");
$mailer->ReplaceBodyKey($additional_your_name, "YOUR_NAME");
$mailer->ReplaceBodyKey($additional_your_phone, "YOUR_PHONE");

$mailer->AddRawAttachment($mailer->Body, "request.html"); // cheat !

if($HasUpload)
	$mailer->AddAttachment($_FILES["property_information_long_legal"]["tmp_name"], $_FILES["property_information_long_legal"]["name"]);

$mailer->Body = "Please view the attached Embassy Title request form for embassytitle.com.";

$mailer->Send();

$loc = "titlework.php?msg=". urlencode("Thank you for ordering Title Work!");
header("Location: $loc");

?>