<?PHP
/****************************************
#	msgboard.php						#
#	Date Updated: 7/22/2011				#
****************************************/

session_start();

include("includes/functions.php");

include("../includes/global_functs.php");

include("../includes/config.php");

dbconn($db['host'], $db['username'], $db['password'], $db['database']);



check();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<script type="text/javascript">
<!--
function MM_popupMsg(msg) { //v1.0
  confirm(msg);
}
//-->
</script>

<title><?PHP echo $config['sitename']; ?> Administration Control Panel</title>

<?PHP	include("./includes/tinymce.php"); ?>



</head>



<body>

<div align='center'><h2><?PHP echo $config['sitename']; ?> Administration Control Panel  - Message Board Administration</h2></div>

<?

if($_GET['action']){


	if($_GET['action'] == "approved"){
		
		$sql="UPDATE msgboard SET approved = 'true' WHERE id = ".$_GET['id']." LIMIT 1 ;";
		
		$result = mysql_query($sql);
		
		//check if query successful
		if($result){
			echo "<h2 style='color:green; align:center;'>Post ID: #".$_GET['id']." has been approved.</h2>";
		} else {
			echo "<h2 style='color:red'>ERROR</h2>";
		}
	}
	
	if($_GET['action'] == "remove"){
		
		$sql="DELETE FROM msgboard WHERE id = ".$_GET['id']." LIMIT 1 ;";
		
		$result = mysql_query($sql);
		
		//check if query successful
		if($result){
			echo "<h2 style='color:red; align:center;'>Post ID: #".$_GET['id']." has been deleted.</h2>";
		} else {
			echo "<h2 style='color:red'>ERROR</h2>";
		}
	}

}


?>

<?php
// find out how many rows are in the table 
$sql3 = "SELECT COUNT(*) FROM msgboard";
$result3 = mysql_query($sql3);
$r = mysql_fetch_row($result3);
$numrows = $r[0];

// number of rows to show per page
$rowsperpage = 10;
// find out total pages
$totalpages = ceil($numrows / $rowsperpage);

// get the current page or set a default
if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
   // cast var as int
   $currentpage = (int) $_GET['currentpage'];
} else {
   // default page num
   $currentpage = 1;
} // end if

// if current page is greater than total pages...
if ($currentpage > $totalpages) {
   // set current page to last page
   $currentpage = $totalpages;
} // end if
// if current page is less than first page...
if ($currentpage < 1) {
   // set current page to first page
   $currentpage = 1;
} // end if

// the offset of the list, based on current page 
$offset = ($currentpage - 1) * $rowsperpage;



$sql2 = "SELECT * FROM msgboard ORDER BY id DESC LIMIT $offset, $rowsperpage;";
$result2 = mysql_query($sql2);

/******  build the pagination links ******/
// range of num links to show
$range = 3;

// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1'>&laquo; Beginning</a> ";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage'>&#8249; Back</a> ";
} // end if 

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " [<b>$x</b>] ";
      // if not current page...
      } else {
         // make it a link
         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x'>$x</a> ";
      } // end else
   } // end if 
} // end for
                 
// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage'>Next &#8250;</a> ";
   // echo forward link for lastpage
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages'>End &raquo;</a> ";
} // end if
/****** end build pagination links ******/


while($rows=mysql_fetch_array($result2)){
?>
<table width="400" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
<tr>
<td><table width="400" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
<tr>
<td width="117">id</td>
<td width="14">:</td>
<td width="357"><?PHP echo $rows['id']; ?></td>
</tr>
<tr>
<td width="117">Name</td>
<td width="14">:</td>
<td width="357"><?PHP echo $rows['name']; ?></td>
</tr>
<tr>
<td width="117">Email</td>
<td width="14">:</td>
<td width="357"><?PHP echo("<a href='mailto:".$rows['email']."'>".$rows['email']."</a>"); ?></td>
</tr>
<tr>
<td valign="top">Message</td>
<td valign="top">:</td>
<td><?PHP echo $rows['comment']; ?></td>
</tr>
<tr>
<td valign="top">Date/Time </td>
<td valign="top">:</td>
<td><?PHP echo $rows['datetime']; ?></td>
</tr>
<tr>
<td width="117">IP</td>
<td width="14">:</td>
<td width="357"><?PHP echo $rows['IP']; ?></td>
</tr>
<!--<tr>
<td width="117">Status:</td>
<td width="14">:</td>
<td width="357">

<?PHP if($rows['approved'] == "true"){ 
	echo("<font style='color:green'>Approved</font>");
} else {
	echo("<font style='color:red'><a onclick=\"MM_popupMsg('Approve this post?')\" href='msgboard.php?action=approved&id=".$rows['id']."'>NOT Approved</a></font>");

}
?>

</td>
</tr>-->

<tr>
<td width="117">Delete:</td>
<td width="14">:</td>
<td width="357">

<?PHP 
	echo("<font style='color:red'><a onclick=\"MM_popupMsg('Are you absolutely sure you want to remove this post? There will be no further confirmation.')\" href='msgboard.php?action=remove&id=".$rows['id']."'>DELETE!</a></font>");

?>

</td>
</tr>

</table></td>
</tr>
</table>
<BR>
<?
}

/******  build the pagination links ******/
// range of num links to show
$range = 3;

// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1'><<</a> ";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage'><</a> ";
} // end if 

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " [<b>$x</b>] ";
      // if not current page...
      } else {
         // make it a link
         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x'>$x</a> ";
      } // end else
   } // end if 
} // end for
                 
// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage'>></a> ";
   // echo forward link for lastpage
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages'>>></a> ";
} // end if
/****** end build pagination links ******/



		echo("<hr>");

		echo("<a href=\"index.php\">Back</a> | <a href=\"logout.php\">Logout</a>");



?>

</body>

</html>

