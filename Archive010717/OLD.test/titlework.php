<?php

require_once("bootloader.php");

$context = getContext(); // sets default contexts
if(isset($_GET["msg"]))
{
	$context["msg"] = $_GET["msg"];
}

echo $twig->render('titlework.html', $context);