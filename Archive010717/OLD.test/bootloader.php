<?php

$base_dir = dirname(__FILE__) . "/";

require_once($base_dir . "Twig/AutoLoader.php");
require_once($base_dir . "includes/config.php");
Twig_Autoloader::register();


$template_dir = $base_dir . "templates/";
$comp_cache_dir = $base_dir . "comp_cache/";

$loader = new Twig_Loader_Filesystem($template_dir);
$twig = new Twig_Environment($loader, array(
    //'cache' => $comp_cache_dir, // turn off caching right now
));

define("A_PHONE", $config['phone']);
define("URL_BASE", $config['url']);

define("SITENAME", $config['sitename']);

function getSettings()
{
	$settings = array();
	$settings["site_name"] = SITENAME;
	$settings["url_base"] = URL_BASE; // add the leading slash
	//$settings["fax"] = "fax";
	$settings["phone"] = A_PHONE;
	$settings["analytics"] = " xxx ";
	return (object)$settings;
}

function getContext()
{
	$context = array(
		'settings' => getSettings(),
	);
	return $context;
}