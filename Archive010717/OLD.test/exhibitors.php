<?php

require_once("bootloader.php");
require_once("includes/db.php");
require_once("includes/global_functs.php");

function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }
  
$year = cleanInput($_GET['year']);

$db = new Db();
$conn = $db->GetNewConnection();

$context = getContext();

//$result = $db->ExecuteQuery("SELECT * FROM sponsors WHERE year = $year ORDER BY linktext", $conn);

$result = $db->ExecuteQuery("SELECT * FROM sponsors ORDER BY linktext", $conn);


$context["sponsors"] = $result;

$db->CloseConnection($conn);

echo $twig->render('sponsors.html', $context);