<?php

/**
 * This sets the admin package as well as checks for login and redirects if required
 */

$template_package = ADMIN_TEMPLATE_PACKAGE;

$require_login_default = true;

$require_login = (isset($require_login)) ? $require_login : $require_login_default;

if($require_login AND !(isset($_SESSION["user.id"]) AND (string)$_SESSION["user.id"] != ""))
{
	redirect(URL_ROOT . "admin/login/");
}