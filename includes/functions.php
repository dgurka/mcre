<?php

// var dump for printing on the web, wraps data in pre tag
function web_var_dump($expression)
{
	echo "<pre>";
	var_dump($expression);
	echo "</pre>";
}

function redirect($loc)
{
	header("Location: $loc");
	exit();
}

function getPageLink($page)
{
	if($page["linkoverride"] != "")
		return $page["linkoverride"];
	else
		return URL_ROOT . "page/" . $page["ID"] . "/";
}

/**
 * renders the dropdown menu!
 * 
 * Could be a bit faster
 */
 


function subpages($pages, $id){
	$matches = [];
	
	foreach($pages as $indx => $page){
		if ($page['menukey'] == $id)
			$matches[] = $page;
	}
	return $matches;
}


function genNavBar($pages, $id, $buff, $depth = 0){
	
	 //prevent infinite loop
	if ($depth >= 10){
		return;
	}
	
	$matches = subpages($pages, $id);
	
	foreach ($matches as $indx => $page){
		
		$link = getPageLink($page);
		
		if (count(subpages($pages,$page['ID'])) > 0) //if it has sub-pages
		{
			$class = "";
			if ($id == "0"){
			    $buff .= "<li class='dropdown menu_{".$page['ID']."}' >\n";
			    $buff .= "<a href='{$link}' class='dropdown-toggle' data-toggle='dropdown'>{$page["title"]} <i class='fa fa-angle-down'></i></a>";
			}else{
			    $buff .= "<li class='dropdown '>\n";
			    $buff .= "<a href='{$link}'>{$page["title"]} <i class='fa fa-angle-right'></i></a>";
			}
			
			
			//<a href='$link'>{$page["title"]}</a>\n";
			$buff .= "<ul class='dropdown-menu' role='menu'>\n";
			$buff = genNavBar($pages,$page['ID'],$buff, $depth);
			
			$buff .= "</ul>\n</li>\n";
		}
		else // if no sub-pages
		{
			$buff .= "<li><a href='$link'>{$page["title"]}</a></li>\n";
		}
	}
	
	return $buff;
}


function getDropdown($is_admin_page)
{
	if($is_admin_page)
		return null;

	$c = Cache::Get("dropdown_menu");
	if($c != null)
		return $c;

	$retval = "";

	$conn = Db::GetNewConnection();

	$menus = Db::ExecuteQuery("SELECT * FROM menu ORDER BY ID", $conn);
	$pages = Db::ExecuteQuery("SELECT * FROM page WHERE menuorder <> -1 ORDER BY menukey, menuorder ASC, ID", $conn);

	Db::CloseConnection($conn);
	
	
	$buff = "<ul class='nav navbar-nav navbar-right'>";
	$buff = genNavBar($pages,0,$buff); //root, set to zero once db has been fixed, all "menu" items should have menukey = 0
	//$buff .= var_export(subpages($pages, 23),true);
	$buff .= "</ul>";
	
	
	
	
	/* //this was the original nav bar generator
	$buff = "<ul class='nav navbar-nav'>";
	foreach ($menus as $i => $menu) 
	{
		$p = array();
		foreach ($pages as $page) 
		{
			if($page["menukey"] == $menu["ID"])
				$p[] = $page;
		}

		if(count($p))
			$link = getPageLink($p[0]);
		else
			$link = "#";

		$buff .= "<li class='menu_{$i}'><a href='$link'>{$menu["name"]}</a>";

		if(count($p) > 1)
		{
			$buff .= "<ul>";
			foreach ($p as $j => $_page) 
			{
				if($j != 0)
				{
					$link = getPageLink($_page);
					$buff .= "<li><a href='$link'>{$_page["title"]}</a>"; //missing </li>?
				}
			}
			$buff .= "</ul>";
		}
		$buff .= "</li>";
	}
	$buff .= "</ul>";
	*/
	
	Cache::Store("dropdown_menu", $buff);
	return $buff;
}



function getDefaultContext($is_admin_page = false)
{
	$app_name = APP_NAME;
	$app_version_code = APP_VERSION_CODE;
	$generatormeta = <<<EOD
<meta name="generator" value="$app_name" />
<meta name="generator_version" value="$app_version_code" />
EOD;
	return array(
		"website_title" => WEBSITE_TITLE,
		"STATIC_ROOT" => STATIC_ROOT,
		"IMAGE_ROOT" => IMAGE_ROOT,
		"CSS_ROOT" => CSS_ROOT,
		"URL_ROOT" => URL_ROOT,
		"EDITABLES_COUNT" => EDITABLES_COUNT,
		"GENERATOR_META" => $generatormeta,
		"DROPDOWN_MENU" => getDropdown($is_admin_page),
		"ADMIN_USER" => (isset($_SESSION["user.id"])) ? $_SESSION["user.id"] : "",
		"ADMIN_ROOT" => (isset($_SESSION["user.root"]) && $_SESSION["user.root"] == true),
		"CAN_ADD_PAGES" => CAN_ADD_PAGES,
		"USE_DIRECTORY" => USE_DIRECTORY,
		"USE_GALLERY" => USE_GALLERY
	);
}

function get($key, $default = null)
{
	if(get_magic_quotes_gpc())
	{
		if(isset($_GET[$key]))
			return stripslashes($_GET[$key]);
		else
			return $default;
	}
	else
	{
		if(isset($_GET[$key]))
			return $_GET[$key];
		else
			return $default;
	}
}

function post($key, $default = null)
{
	if(get_magic_quotes_gpc())
	{
		if(isset($_POST[$key]))
			return stripslashes($_POST[$key]);
		else
			return $default;
	}
	else
	{
		if(isset($_POST[$key]))
			return $_POST[$key];
		else
			return $default;
	}
}

function q($str)
{	
	return str_replace("\"", "&quote;", $str);
}

function makeOptions($arr, $default = "")
{
	$options = array();
	foreach ($arr as $key => $value) 
	{
		$o = "<option value=\"" . $key . "\"";
		if((string)$default === (string)$key)
			$o .= "selected='selected'";
		$o .= ">" . $value . "</option>";
		$options[] = $o;
	}
	return implode("", $options);
}

/**
 * Awesome function canabalized and modified from picNbid
 */
function saveFile($filed, $cat)
{
	if(!isset($_FILES[$filed]))
		return;

	$file = $_FILES[$filed];

	if($file["name"] == "")
		return false;
	
	$dest = "images/" . $cat . "/" . uniqid() . "." . $file["name"];
	$dest = str_replace(" ", "_", $dest);

	if(!file_exists(dirname(BASE_DIR . $dest)))
		mkdir(dirname(BASE_DIR . $dest), "777", true);

	move_uploaded_file($file["tmp_name"],  BASE_DIR . $dest);

	return $dest;
}

/**
 * below are the date conversion functions
 */

function dbDate($date)
{
	if($date != "")
	{
		$_t = strtotime($date);
		return "'" . date("Y-m-d", $_t) . "'";
	}
	else
	{
		return "NULL";
	}
}

function disDate($date)
{
	if($date === null OR $date == "")
	{
		return "";
	}
	else
	{
		$_t = strtotime($date);
		return date("m/d/Y", $_t);
	}
}

function dbTime($time)
{
	if($time != "")
	{
		$_t = strtotime($time);
		return "'" . date("H:i:s", $_t) . "'";
	}
	else
	{
		return "NULL";
	}
}

function disTime($time)
{
	if($time === null OR $time == "")
	{
		return "";
	}
	else
	{
		$_t = strtotime($time);
		return date("g:i A", $_t);
	}
}

define('GEOURL', 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=');

class GeoCoder
{
	public static function getGeoCoding($addr)
	{
		$coding = new stdClass();
		
		$jsn = json_decode(file_get_contents(GEOURL.urlencode($addr)), true);
		
		if(!isset($jsn["results"][0]["geometry"]["location"]["lat"]) || 
		   !isset($jsn["results"][0]["geometry"]["location"]["lng"]))
		   return null;
		$coding->lat = $jsn["results"][0]["geometry"]["location"]["lat"];
		$coding->lng = $jsn["results"][0]["geometry"]["location"]["lng"];
		
		return $coding;
	}
}
?>