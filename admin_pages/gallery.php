<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$gid = (int)$matches[1];

if(get("delete") != null)
{
	$id = (int)get("delete");

	Db::ExecuteNonQuery("DELETE FROM gallery_image WHERE ID = '$id'", $conn);

	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/gallery/{$gid}/");
}

if(get("move") != null)
{
	$id = (int)get("move");
	$dir = get("dir");

	$images = Db::ExecuteQuery("SELECT * FROM gallery_image WHERE gallery = '$gid' ORDER BY `order` DESC, ID", $conn);

	$len = count($images);
	for ($i=0; $i < $len; $i++) 
	{ 
		if($images[$i]["ID"] == $id)
		{
			if($dir == "up")
			{
				if($i == 0)
				{
					// break
					Db::CloseConnection($conn);
					redirect(URL_ROOT . "admin/gallery/{$gid}/");
				}
				$tmp = $images[$i];
				$nkey = (int)$i-1;
				$images[$i] = $images[$nkey];
				$images[$nkey] = $tmp;
			}
			else
			{
				if($i == $len-1)
				{
					// break
					Db::CloseConnection($conn);
					redirect(URL_ROOT . "admin/gallery/{$gid}/");
				}
				$tmp = $images[$i];
				$nkey = (int)$i+1;
				$images[$i] = $images[$nkey];
				$images[$nkey] = $tmp;
			}
			break;
		}
	}

	for ($i=0; $i < count($images); $i++) 
	{
		$place = (count($images) - $i);
		$_id = $images[$i]["ID"];
		Db::ExecuteNonQuery("UPDATE gallery_image SET `order` = $place WHERE ID = $_id", $conn);
	}

	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/gallery/{$gid}/");
}

if($_SERVER["REQUEST_METHOD"] == "POST" && post("action") == "edit")
{
	$caption = Db::EscapeString(post("caption"), $conn);
	$id = Db::EscapeString(post("id"), $conn);

	Db::ExecuteNonQuery("UPDATE gallery_image SET caption = '$caption' WHERE ID = '$id'", $conn);

	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/gallery/{$gid}/");
}

if($_SERVER["REQUEST_METHOD"] == "POST" && post("action") == "upload")
{
	// adding a new image
	$caption = Db::EscapeString(post("caption"), $conn);

	$imgloc = saveFile("image", "gallery_images");
	
	if($imgloc !== false)
		$imgloc = "$imgloc";
	else
		$imgloc = "";
	
	if($imgloc != "")
	{
		Db::ExecuteNonQuery("INSERT INTO gallery_image (gallery, `order`, caption, imgloc) VALUES ('$gid', 0, '$caption', '$imgloc')", $conn);
		Db::CloseConnection($conn);
		redirect(URL_ROOT . "admin/gallery/{$gid}/");
	}
	else
	{
		$context["err"] = "Please upload an image";
	}
}

$images = Db::ExecuteQuery("SELECT * FROM gallery_image WHERE gallery = '$gid' ORDER BY `order` DESC, ID", $conn);

$context["images"] = $images;

Db::CloseConnection($conn);

echo $twig->render('gallery.html', $context);