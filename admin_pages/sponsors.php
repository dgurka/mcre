<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$sponsors_query = Db::ExecuteQuery("SELECT * FROM sponsors ORDER BY linktext", $conn);

$pagecontent = "";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$linktext = Db::EscapeString(post("linktext"), $conn);
	$linkurl = Db::EscapeString(post("linkurl"), $conn);
	$linktext_edit = Db::EscapeString(post("linktext_edit"), $conn);
	$linkurl_edit = Db::EscapeString(post("linkurl_edit"), $conn);
	$sponsorID = Db::EscapeString(post("id"), $conn);
	$replaceimg = Db::EscapeString(post("replaceimg"), $conn);
	$dispmsg = "Your changes have been saved.";
	
	// are we updating...
	if($sponsorID != "") // was isset(), but gave false positives
	{
		/*//$getID = (int)$matches[1];

		$editquery = "UPDATE sponsors SET linktext = '$linktext_edit', linkurl = '$linkurl_edit' WHERE ID = $sponsorID";

		Db::ExecuteNonQuery($editquery, $conn);
		$pagecontent .= "<span style=\"font-weight:bold; color:green;\">Sponsor Updated!</span><br/>";
		//$pagecontent .= "DIAG: linktext: ".$linktext."<br/> linkurl: ".$linkurl."linktext_edit: ".$linktext_edit."<br/> linkurl_edit: ".$linkurl_edit."<br /> sponsorID: ".$sponsorID."<br />";*/
		
		if($replaceimg == "yes"){ // is there being an image posted?
			$target_path = BASE_DIR . "inventory/";
			
			$target_path = $target_path . basename( $_FILES['uploadedfile_update']['name']); 
			
			$file_path = basename( $_FILES['uploadedfile_update']['name']); 
			$file_size = basename( $_FILES['uploadedfile_update']['size']); 
			
			$imgurl_edit = URL_ROOT . "inventory/" . $file_path;
			
			if(move_uploaded_file($_FILES['uploadedfile_update']['tmp_name'], $target_path)) {		
				$editquery1 = "UPDATE sponsors SET linktext = '$linktext_edit', linkurl = '$linkurl_edit',  imgurl='$imgurl_edit' WHERE ID = $sponsorID";
				Db::ExecuteNonQuery($editquery1, $conn);
			
				$pagecontent .= "<span style=\"font-weight:bold; color:green;\">Sponsor Updated!</span>";		
			} else{
				$pagecontent .= "<span style=\"font-weight:bold; color:green;\">There was an error uploading the file, please try again!</span>";
			}
		} else { // no image
			$editquery2 = "UPDATE sponsors SET linktext = '$linktext_edit', linkurl = '$linkurl_edit' WHERE ID = $sponsorID";
		
			Db::ExecuteNonQuery($editquery2, $conn);
			$pagecontent .= "<span style=\"font-weight:bold; color:green;\">Sponsor Updated!</span><br/>";
		
		}   
		
	}
	else //... or adding a new entry
	{
		$target_path = BASE_DIR . "inventory/";
		
		$target_path = $target_path . basename( $_FILES['uploadedfile']['name']); 
		
		$file_path = basename( $_FILES['uploadedfile']['name']); 
		$file_size = basename( $_FILES['uploadedfile']['size']); 
		
		$imgurl = URL_ROOT . "inventory/" . $file_path;
		
		if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {			
			$addquery = "INSERT INTO sponsors (linktext, linkurl, imgurl) VALUES ('$linktext', '$linkurl', '$imgurl');";
			
			Db::ExecuteNonQuery($addquery, $conn);
			//$id = Db::GetLastInsertID($conn);
			$pagecontent .= "<span style=\"font-weight:bold; color:green;\">Sponsor Added!</span>";		
		} else{
			$pagecontent .= "<span style=\"font-weight:bold; color:green;\">There was an error uploading the file, please try again!</span>";
		}
	}
} 

	$pagecontent .= "<form enctype='multipart/form-data' action='". URL_ROOT ."admin/sponsors/' method='POST'><p><!-- error --><br>Select an image:<br><br><input name='uploadedfile' type='file' /></p><p align='left'>Exhibitor Name:<br><input name='linktext' type='text' id='linktext' size='45'><br />Sponsor URL:<br><input name='linkurl' type='text' id='linkurl' value='http://' size='45'><input type='hidden' name='id' value=''><input name='Submit' type='submit' id='Submit' value='Add Sponsor' /></p></form>";

foreach ($sponsors_query as $value) 
{
	
	$linktext = $value["linktext"];
	$linkurl = $value["linkurl"];
	$imgurl = $value["imgurl"];
	$id = $value["ID"];	
	
	$pagecontent .= '<fieldset><table><tr><td rowspan="2"><a href="../../'. $imgurl .'" target="_blank"><img border="0" style="float:left;" width="65" height="65" src="'. $imgurl .'"></a></td><td><form action="' . URL_ROOT . 'admin/sponsors/" method="post" action="post" enctype="multipart/form-data">
	<input style="margin-left:20px;" name="linktext_edit" value="'. $linktext .'" type="text" id="linktext_edit" size="45"><br /><input style="margin-left:20px;" name="linkurl_edit" value="'. $linkurl .'" type="text" id="linkurl_edit" size="45"><input type="hidden" name="id" value="'.$id.'"></td><td rowspan="2"></td></tr>
	  <tr>
		<td> 
	<input style="margin-left:20px;" name="uploadedfile_update" id="uploadedfile_update" type="file" /> <input name="replaceimg" type="checkbox" id="replaceimg" value="yes"> Replace Image?<br /><br /><br />

<input type="submit" name="Submit" id="Submit" value="Update Sponsor">

<a style="margin-left:20px; font-weight:bold; color:red;" href="../sponsors/delete/'. $id .'" ­ onclick="return confirm(\'Are you sure you want to delete this sponsor? This action cannot be undone!\')">DELETE SPONSOR</a> 
	</form><br /><hr /></td>
		</tr>
	</table>
	</fieldset>';

}




$context["pagecontent"] = $pagecontent;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('sponsors.html', $context);