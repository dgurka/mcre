<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

//$menus = Db::ExecuteQuery("SELECT * FROM videos WHERE menukey = 0 ORDER BY menuorder ASC", $conn);
$menus = Db::ExecuteQuery("SELECT * FROM videos WHERE menukey = 0 ORDER BY title ASC", $conn);

$vidmenu = "<ul>";

foreach ($menus as $value) 
{
	
	$menukey = $value["ID"];
	//$vids = Db::ExecuteQuery("SELECT ID, title FROM videos WHERE menukey = $menukey ORDER BY menuorder DESC, ID", $conn);
	
	$vidmenu .= "<li>" . $value["title"] . "  <button class='btn' onclick='editVideo(".$value['ID'].")'>edit</button>"; 
	
	$vidmenu .= " <button class='btn' onclick='deleteVideo(".$value['ID'].")'>Delete</button>";

	
	/*if ($value['menuorder'] > 1)
		$vidmenu .= " <button class='btn' onclick='moveUp(".$value['ID'].")'>Up</button>";

	if ($value['menuorder'] < count($menus))
		$vidmenu .= " <button class='btn' onclick='moveDown(".$value['ID'].")'>Down</button>";*/

}

$vidmenu .= "</ul>";

/*
//$featured_vids = Db::ExecuteQuery("SELECT ID, title FROM videos WHERE featured = 0 ORDER BY ID", $conn);
$featured_vids = Db::ExecuteQuery("SELECT * FROM videos WHERE featured = 0 ORDER BY menuorder ASC", $conn);

if(count($featured_vids))
{
	$vidmenu .= "<h3>Featured Videos</h3><ul>";
	foreach ($featured_vids as $i => $vid) 
	{
		$vidmenu .= "<li>" . $vid["title"];
		$vidid = $vid["ID"];

		$vidmenu .= " <button class='btn' onclick='editVideo($vidid)'>edit</button>";
		
		if ($vid['menuorder'] > 1)
			$vidmenu .= " <button class='btn' onclick='moveUp(".$vid['ID'].")'>Up</button>";

		if ($vid['menuorder'] < count($menus))
			$vidmenu .= " <button class='btn' onclick='moveDown(".$vid['ID'].")'>Down</button>";

		$vidmenu .= " <button class='btn' onclick='deleteVideo($vidid)'>Delete</button>";


		$vidmenu .= "</li>";
	}

	$vidmenu .= "</ul>";
}
*/
//$vids = Db::ExecuteQuery("SELECT ID, title FROM videos WHERE menukey = -1 ORDER BY ID", $conn);
$vids = Db::ExecuteQuery("SELECT ID, title FROM videos WHERE menukey = -1 ORDER BY title ASC", $conn);

if(count($vids))
{
	$vidmenu .= "<h3>Hidden Videos</h3><ul>";
	foreach ($vids as $i => $vid) 
	{
		$vidmenu .= "<li>" . $vid["title"];
		$vidid = $vid["ID"];

		$vidmenu .= " <button class='btn' onclick='editVideo($vidid)'>edit</button>";

		//$vidmenu .= " <button class='btn' onclick='deleteVideo($vidid)'>edit</button>";

		$vidmenu .= " <button class='btn' onclick='deleteVideo($vidid)'>Delete</button>";
		
		$vidmenu .= "</li>";
	}

	$vidmenu .= "</ul>";
}


$context["pagemenu"] = $vidmenu;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('videos.html', $context);