<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$id = (int)$matches[1];


Db::ExecuteNonQuery("DELETE FROM sponsors WHERE ID = $id", $conn);

redirect(URL_ROOT . "admin/sponsors/");