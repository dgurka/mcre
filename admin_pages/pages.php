<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$menus = Db::ExecuteQuery("SELECT * FROM page WHERE menukey = 0 ORDER BY ID", $conn);

$pagemenu = "";

foreach ($menus as $value) 
{

	$menukey = $value["ID"];
	$pages = Db::ExecuteQuery("SELECT ID, title FROM page WHERE menukey = $menukey ORDER BY menuorder DESC, ID", $conn);
	if(count($pages))
	{
		$pagemenu .= "<h3>" . $value["title"] . "  <button class='btn' onclick='editPage(".$value['ID'].")'>edit</button></h3>";

		$pagemenu .="<ul>";
		foreach ($pages as $i => $page) 
		{
			$pagemenu .= "<li>" . $page["title"];
			$pageid = $page["ID"];

			$pagemenu .= " <button class='btn' onclick='editPage($pageid)'>edit</button>";

			if($i != 0)
				$pagemenu .= " <button class='btn' onclick='moveUp($pageid)'>Up</button>";

			if($i + 1 != count($pages))
				$pagemenu .= " <button class='btn' onclick='moveDown($pageid)'>Down</button>";


			$pagemenu .= "<ul>";
			$subpages = Db::ExecuteQuery("SELECT ID, title FROM page WHERE menukey = ".$pageid." ORDER BY menuorder DESC, ID", $conn);
				foreach ($subpages as $i => $subpage) 
				{
					$pagemenu .= "<li style='list-style-type:none;'> &mdash; " . $subpage["title"];
					$subpageid = $subpage["ID"];
			
					$pagemenu .= " <button class='btn' onclick='editPage(".$subpage["ID"].")'>edit</button>";
			
					if($i != 0)
						$pagemenu .= " <button class='btn' onclick='moveUp(".$subpage["ID"].")'>Up</button>";
			
					if($i + 1 != count($pages))
						$pagemenu .= " <button class='btn' onclick='moveDown(".$subpage["ID"].")'>Down</button>";
			
					$pagemenu .= "</li>";
				}
				


			$pagemenu .= "</ul></li>";
		}
			
		$pagemenu .= "</ul>";
	} else {
				$pagemenu .= "<h3>" . $value["title"] . "  <button class='btn' onclick='editPage(".$value['ID'].")'>edit</button></h3>";
				$pagemenu .= "<ul><li>No Sub Pages to Show</li></ul>";
	}
}


$pages = Db::ExecuteQuery("SELECT ID, title FROM page WHERE menukey = -1 ORDER BY ID", $conn);

if(count($pages))
{
	$pagemenu .= "<h3>Other Pages</h3><ul>";
	foreach ($pages as $i => $page) 
	{
		$pagemenu .= "<li>" . $page["title"];
		$pageid = $page["ID"];

		$pagemenu .= " <button class='btn' onclick='editPage($pageid)'>edit</button>";

		$pagemenu .= "</li>";
	}

	$pagemenu .= "</ul>";
}


$context["pagemenu"] = $pagemenu;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('index.html', $context);