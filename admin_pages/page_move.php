<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$id = (int)$matches[1];

$menukey = Db::ExecuteFirst("SELECT menukey FROM page WHERE ID = $id", $conn);

$menukey = $menukey["menukey"];

$pages = Db::ExecuteQuery("SELECT ID FROM page WHERE menukey = $menukey ORDER BY menuorder DESC, ID", $conn);

/*web_var_dump($pages);
exit();*/

$len = count($pages);
for ($i=0; $i < $len; $i++) 
{ 
	if($pages[$i]["ID"] == $id)
	{
		if($matches[2] == "up")
		{
			$tmp = $pages[$i];
			$nkey = (int)$i-1;
			$pages[$i] = $pages[$nkey];
			$pages[$nkey] = $tmp;
		}
		else
		{
			$tmp = $pages[$i];
			$nkey = (int)$i+1;
			$pages[$i] = $pages[$nkey];
			$pages[$nkey] = $tmp;
		}
		break;
	}
}

for ($i=0; $i < count($pages); $i++) 
{
	$place = (count($pages) - $i);
	$_id = $pages[$i]["ID"];
	Db::ExecuteNonQuery("UPDATE page SET menuorder = $place WHERE ID = $_id", $conn);
}
redirect(URL_ROOT . "admin/pages/");